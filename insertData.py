import json
from datetime import datetime
from grakn.client import GraknClient

import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--file', default="res.json", help='Specify data file to process')
parser.add_argument('--keyspace', default="experiment", help='Specify Grakn keyspace')
parser.add_argument('--host', default="localhost:48555", help='Specify Grakn host:port')
parser.add_argument('--limit', type=int, default=100, help='Limit number of records to process')
parser.add_argument('--verbose', type=int, default=0, help='Verbose mode;')

args = parser.parse_args()

f = open(args.file,'r')
raw_data = f.readlines()[0]
f.close()

data = json.loads(raw_data)

print "Found: " + str(len(data)) + " records in dataset\nThe limit is %s" % args.limit
if bool(args.verbose):
	print "Verbose mode is on"


def processLocation(item):
	query=''
	if 'location' in item:
		query += 'insert $l isa location,'
		query += 'has address "%s";' % item['location']		
		doQuery(query)
		query = 'match $u isa user, has user-id %s;' % item['user_id']
		query += '$l isa location, has address "%s";' % item['location']
		query += 'insert $r (located-user: $u, user-location: $l) isa location-of-user;'
		doQuery(query)


def processAchievements(item):
	def createBadge(color):
		query = 'insert $b isa badge,'
		query += 'has color "%s";' % color
		doQuery(query)
	if 'badge_counts' in item:
		for (color, tally) in item['badge_counts'].items():
				createBadge(color)
				query = 'match '
				query += '$u isa user, has user-id %s;' % item['user_id']
				query += '$b isa badge, has color "%s";' % color
				query += 'insert $award-badge (contributor: $u, award: $b) isa achievement,' 
				query += 'has score %s;' % tally 
				doQuery(query)

def processUser(item):
	query = 'insert $u isa user,'
	query += 'has user-id %s,' % item['user_id']
	query += 'has name "%s",' % item['display_name'] 
	if bool(args.verbose):
		print item['display_name']
	query += 'has reputation %s,' % item['reputation']
	if item['is_employee']:
 	 	query += 'has is-employee true,'
 	else:
 	 	query += 'has is-employee false,'
	query += 'has last-modified %s,' % datetime.fromtimestamp(item['last_modified_date']).utcnow().isoformat()
	query += 'has last-accessed %s,' % datetime.fromtimestamp(item['last_access_date']).utcnow().isoformat()
	if 'timed_penalty_date' in item:
 			query += 'has penalty-until %s,' % datetime.fromtimestamp(item['timed_penalty_date']).utcnow().isoformat()
	query += 'has created %s,' % datetime.fromtimestamp(item['creation_date']).utcnow().isoformat()
	if 'accept_rate' in item:	
		query += 'has accept-rate %s,' % item['accept_rate']
	if 'age' in item:
		query += 'has age %s,' % item['age']
	if 'link' in item:
		query += 'has url "%s",' % item['link']
	if 'website' in item:
		query += 'has website "%s",' % item['website']
	if 'about' in item:
		query += 'has about "%s",' % item['about']
	if 'up_vote_count' in item:
		query += 'has up-vote %s,' % item['up_vote_count']
	if 'down_vote_count' in item:
		query += 'has down-vote %s,' % item['down_vote_count']
	if 'view_count' in item:
		query += 'has view-count %s,' % item['view_count']
	if 'answer_count' in item:
		query += 'has answer-count %s,' % item['answer_count']	
	query += 'has account-id %s,' % item['account_id']
	query += 'has user-type "%s",' % item['user_type']
	query += 'has profile-image "%s";' % item['profile_image']
	doQuery(query)


def processRecord(item):
	processUser(item)
	processLocation(item)
	processAchievements(item)
	# end of processing

def doQuery(query):
	if bool(args.verbose):
		print query
	try:
		with GraknClient(uri=args.host) as client:
			with client.session(keyspace=args.keyspace) as 		session:
				## session is open
				## Execute a query using a WRITE transaction
				with session.transaction().write() as write_transaction:
					insert_iterator = write_transaction.query(query)
					concepts = insert_iterator.collect_concepts()
					if bool(args.verbose):
						print("Inserted a record with ID: {0}".format(concepts[0].id))
					write_transaction.commit()
			pass
	except:
		pass
		## session is closed
	## client is closed

i=0
for item in data:
	if i > args.limit:
		break
	processRecord(item)
	i=i+1