package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"
)

const requestURL = "https://api.stackexchange.com/2.2/users?pagesize=100&fromdate=1514764800&todate=1564531200&order=desc&sort=reputation&site=stackoverflow&filter=!-*jbN*IioeFP&page=%d"

func get(page int, data chan []APIResponseItem) (int, error) {
	var responseData APIResponse
	fmt.Println("GET Response:", fmt.Sprintf(requestURL, page))
	response, err := http.Get(fmt.Sprintf(requestURL, page))
	if err != nil || response.StatusCode != 200 {
		return page, err
	}
	defer response.Body.Close()
	err = json.NewDecoder(response.Body).Decode(&responseData)
	if err != nil {
		return page, err
	}
	fmt.Println("Downloaded", len(responseData.Items), "items for page", page)
	if len(responseData.Items) != 0 {
		data <- responseData.Items
		if responseData.HasMore && responseData.QuotaRemaining > 0 {
		} else {
			return -1, fmt.Errorf("Out of Quota or end of data; %b, %d", responseData.HasMore, responseData.QuotaRemaining)
		}
	}
	return page + 1, nil
}

func main() {
	var delay = flag.Int("delay", 30, "Delay between requests in seconds")
	var savePath = flag.String("save-to", "./data.json", "Path to save data")
	var startPage = flag.Int("from", 1, "Define start page")
	var endPage = flag.Int("to", 1, "Define final page")
	flag.Parse()
	fmt.Println("Downloading from page", *startPage, "to", *endPage, "with delay", *delay, "seconds; saving to", *savePath)

	dataFile, err := os.OpenFile(*savePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer dataFile.Close()

	done := make(chan struct{})
	items := make(chan []APIResponseItem)
	go func() {
		go func() {
			for item := range items {
				b, err := json.Marshal(item)
				if err != nil {
					fmt.Printf("Error: %s\n", err)
					done <- struct{}{}
					return
				}
				dataFile.Write(b)
			}
			fmt.Println("Data chan was closed")
			done <- struct{}{}
		}()
		for p := *startPage; p < *endPage+1; p++ {
			fmt.Println("Current page is", p, "; will stops in", *endPage+1-p, "steps")
			np, err := get(p, items)
			if np == -1 || err != nil {
				fmt.Println(err)
				fmt.Println("Closing the data chan because of error", err, "on page", p)
				close(items)
				break
			}
			rand.Seed(time.Now().UnixNano() + int64(p))
			mixedDelay := rand.Intn(4) + 1
			fmt.Println("Waiting for", *delay+mixedDelay, "seconds")
			time.Sleep(time.Duration(*delay) * time.Second)
		}
		fmt.Println("Closing the data chan because it's end of the loop")
		close(items)
	}()
	<-done
	fmt.Println("Got the done signal")
}
