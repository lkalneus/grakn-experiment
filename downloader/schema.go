package main

// APIResponse describes stackoverflow API response
type APIResponse struct {
	Items          []APIResponseItem `json:"items"`
	HasMore        bool              `json:"has_more"`
	Backoff        int               `json:"backoff"`
	QuotaMax       int               `json:"quota_max"`
	QuotaRemaining int               `json:"quota_remaining"`
}

// APIResponseItem describes user info
type APIResponseItem struct {
	BadgeCounts struct {
		Bronze int `json:"bronze"`
		Silver int `json:"silver"`
		Gold   int `json:"gold"`
	} `json:"badge_counts"`
	AccountID               int    `json:"account_id"`
	IsEmployee              bool   `json:"is_employee"`
	LastModifiedDate        int    `json:"last_modified_date"`
	LastAccessDate          int    `json:"last_access_date"`
	ReputationChangeYear    int    `json:"reputation_change_year"`
	ReputationChangeQuarter int    `json:"reputation_change_quarter"`
	ReputationChangeMonth   int    `json:"reputation_change_month"`
	ReputationChangeWeek    int    `json:"reputation_change_week"`
	ReputationChangeDay     int    `json:"reputation_change_day"`
	Reputation              int    `json:"reputation"`
	CreationDate            int    `json:"creation_date"`
	UserType                string `json:"user_type"`
	UserID                  int    `json:"user_id"`
	Location                string `json:"location,omitempty"`
	WebsiteURL              string `json:"website_url,omitempty"`
	Link                    string `json:"link"`
	ProfileImage            string `json:"profile_image"`
	DisplayName             string `json:"display_name"`
	AcceptRate              int    `json:"accept_rate,omitempty"`
	ViewCount               int    `json:"view_count"`
	DownVoteCount           int    `json:"down_vote_count"`
	UpVoteCount             int    `json:"up_vote_count"`
	AnswerCount             int    `json:"answer_count"`
	QuestionCount           int    `json:"question_count"`
	AboutMe                 string `json:"about_me,omitempty"`
}
